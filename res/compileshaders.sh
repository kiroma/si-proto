#!/bin/sh
glslc --target-env=opengl -c -O basic.frag basic.vert instanced.vert textureless.frag textureless.vert instancedCompressed.vert
spirv-link basic.frag.spv basic.vert.spv -o basic.spv
spirv-link basic.frag.spv instanced.vert.spv -o instanced.spv
spirv-link textureless.frag.spv textureless.vert.spv -o textureless.spv
spirv-link basic.frag.spv instancedCompressed.vert.spv -o instancedCompressed.spv

spirv-opt basic.spv -o basic_opt.spv
spirv-opt instanced.spv -o instanced_opt.spv
spirv-opt textureless.spv -o textureless_opt.spv
spirv-opt instancedCompressed.spv -o instancedCompressed_opt.spv
