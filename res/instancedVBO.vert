#version 430 core
layout(location = 0) in vec2 vPos;
layout(location = 1) in vec2 vTexPos;
layout(location = 2) in vec4 model1;
layout(location = 3) in vec4 model2;
layout(location = 4) in vec4 model3;
layout(location = 5) in vec4 model4;

layout(location = 2) out vec2 texPos;

layout(binding = 0, std140) uniform PV
{
	mat4 projection;
	mat4 view;
};

void main()
{
	mat4 model = mat4(model1, model2, model3, model4);
	gl_Position = projection * view * model * vec4(vPos, 0, 1);
	texPos = vTexPos;
}
