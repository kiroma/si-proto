#version 430 core
layout(location = 2) in vec2 texPos;
layout(location = 0) out vec4 FragColor;

uniform sampler2D tex;

void main()
{
	FragColor = texture(tex, texPos);
}
