#version 430 core
layout(location = 0) in vec2 vPos;

layout(binding = 0, std140) uniform PV
{
	mat4 projection;
	mat4 view;
};

layout(location = 2) uniform mat4 model;

void main()
{
	gl_Position = projection * view * model * vec4(vPos, 0, 1);
}
