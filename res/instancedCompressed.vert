#version 430 core
layout(location = 0) in vec2 vPos;
layout(location = 1) in vec2 vTexPos;
layout(location = 2) out vec2 texPos;

layout(binding = 0, std140) uniform PV
{
	mat4 projection;
	mat4 view;
};

layout(binding = 0, std430) restrict readonly buffer mainBuffer 
{
	mat3x2 model[];
};

void main()
{
	const mat3x2 currentModel = model[gl_InstanceID];
	mat4 fullModel = mat4(1);
	fullModel[0].xy = currentModel[0];
	fullModel[1].xy = currentModel[1];
	fullModel[3].xy = currentModel[2];
	gl_Position = projection * view * fullModel * vec4(vPos, 0, 1);
	texPos = vTexPos;
}
