#version 430 core
layout(location = 0) in vec2 vPos;
layout(location = 1) in vec2 vTexPos;
layout(location = 2) out vec2 texPos;

layout(binding = 0, std140) uniform PV
{
	mat4 projection;
	mat4 view;
};

layout(binding = 0, std140) restrict readonly buffer mainBuffer
{
	mat4 model[];
};

void main()
{
	gl_Position = projection * view * model[gl_InstanceID] * vec4(vPos, 0, 1);
	texPos = vTexPos;
}
