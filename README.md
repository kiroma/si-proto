# SI-Proto
Basically a shoot-em-up
Still not feature complete

## Options
- `-armageddon <number>` increases spawn rate of enemies times number
- `-impossible <number>` increases enemy's fire rate times number
- `-ivan <number>` increases player's firing speed times number
- `-ghost` disables collisions on player, changes displayed text from health and score to frametime and bullet count
- `-f <number>` changes the FPS limit (120fps by default)
- `-god <number>` adds a bunch of health to you
- `-subframes <number>` how many simulation subframes to run per frame
- `-shakes <number>` affects screen shake effect strenght
- `-shakea <number>` affects attenuation of the screen shake effect

## Compiling
You'll need SFML and glm.  
Run `cmake -H. -Bbuild -DCMAKE_BUILD_TYPE=Release`, `cd build`, `make`, and you'll get the `siproto` executable.
