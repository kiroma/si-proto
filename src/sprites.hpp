#pragma once

#include "entity.hpp"
#include "pgl.hpp"

class Bullet : public Entity
{
	float speed = NAN;

public:
	constexpr Bullet(glm::vec2 position, float rotation, float speed = 2500.0f) :
	Entity(BoundingBox{position, glm::vec2(0.125f, 1.0f) * 16.0f}, rotation),
	speed(speed)
	{
	}
	constexpr void update(float deltaTime)
	{
		setPosition(position - (glm::vec2(-s, c) * deltaTime * speed));
	}
};

class Player : public Entity
{
	pgl::Program& program;
	pgl::vao vao;
	pgl::data::vbo vbo;
	pgl::data::ebo ebo;
	pgl::storage::ssbo ssbo;
	pgl::Texture& texture;
	int health;

public:
	Player(int health);
	void render();
	int getHealth() const;
	void hit();
};

class Enemy : public Entity
{
	static constexpr float speed{150.0f};
	int health = 3;
	float firingClock = 0.0f;
	float fireRate;

public:
	Enemy(const glm::vec2 position, const float rotation, float fireRate = 60.0f);
	void update(const float deltaTime);
	int getHealth() const;
	bool isFiring() const;
	void fired();
	void hit();
	float getFiringTime();
};
