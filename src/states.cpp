#include "states.hpp"

MainState::MainState(Settings settings) :
	settings(settings),
	player(settings.playerHealth),
	enemies(settings, "res/spawns.txt"),
	mt(std::random_device()()),
	healthText(GL_STREAM_DRAW),
	scoreText(GL_STREAM_DRAW)
{
	//backgroundMusic.openFromFile("res/bgmusic.ogg");
	//backgroundMusic.setAttenuation(0);
	//backgroundMusic.play();
	sharedUbo.reserve<glm::mat4>(2, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT, true);
	static_cast<glm::mat4*>(sharedUbo.getMappedPtr())[1] = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	sharedUbo.flushBuffer<glm::mat4>(1);
	healthText.setSize(16.0f);
	healthText.setBackgroundColour({0.0f, 0.0f, 0.0f, 0.5f});
	scoreText.setSize(16.0f);
	scoreText.setBackgroundColour({0.0f, 0.0f, 0.0f, 0.5f});
}

std::pair<State::Status, std::unique_ptr<State>> MainState::run(sf::Window& window)
{
	clock.restart();
	sharedUbo.bindBufferBase(0);
	window.setMouseCursorVisible(false);
	bool playerFiring = false;
	while(window.isOpen())
	{
		{
			sf::Event event;
			while(window.pollEvent(event))
			{
				switch(event.type)
				{
				case sf::Event::LostFocus:
				case sf::Event::GainedFocus:
				case sf::Event::TextEntered:
				case sf::Event::KeyReleased:
				case sf::Event::MouseWheelMoved:
				case sf::Event::MouseWheelScrolled:
				case sf::Event::MouseEntered:
				case sf::Event::MouseLeft:
				case sf::Event::JoystickButtonPressed:
				case sf::Event::JoystickButtonReleased:
				case sf::Event::JoystickMoved:
				case sf::Event::JoystickConnected:
				case sf::Event::JoystickDisconnected:
				case sf::Event::TouchBegan:
				case sf::Event::TouchMoved:
				case sf::Event::TouchEnded:
				case sf::Event::SensorChanged:
				case sf::Event::Count: // This isn't even an event
					break; // We don't handle those events
				case sf::Event::KeyPressed:
					switch(event.key.code)
					{
					case sf::Keyboard::Key::Escape:
						return {Status::pop, nullptr};
					default:
						break;
					}
					break;
				case sf::Event::Closed:
					return {Status::pop, nullptr};
					break;
				case sf::Event::Resized:
					glViewport(0, 0, event.size.width, event.size.height);
					break;
				case sf::Event::MouseMoved:
					player.setPosition({event.mouseMove.x, event.mouseMove.y});
					break;
				case sf::Event::MouseButtonPressed:
					if(event.mouseButton.button == sf::Mouse::Button::Left)
					{
						playerFiring = true;
					}
					break;
				case sf::Event::MouseButtonReleased:
					if(event.mouseButton.button == sf::Mouse::Button::Left)
					{
						playerFiring = false;
					}
					break;
				}
			}
		}
		auto start = std::chrono::steady_clock::now();
		float deltaTime = clock.restart().asSeconds();
		weaponTimer += deltaTime;
		glm::mat4* ptr = reinterpret_cast<glm::mat4*>(sharedUbo.getMappedPtr());
		ptr[0] = glm::ortho<float>(0, window.getSize().x, window.getSize().y, 0, 0.0f, 10.0f);
		sharedUbo.flushBuffer<glm::mat4>(0, 1);
		if(shake > 0.0f)
		{
			shake -= deltaTime * shake * settings.shake.attenuation;
			if(shake <= 0.0f)
			{
				shake = 0.0f;
			}
			std::uniform_real_distribution urd(-1.0f, 1.0f);

			glm::vec2 rand(urd(mt), urd(mt));
			rand = glm::normalize(rand);
			ptr[1] = glm::translate(glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)), glm::vec3(rand * settings.shake.strength * shake, 0));
		}
		else
		{
			ptr[1] = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
		}
		sharedUbo.flushBuffer<glm::mat4>(1, 1);
		glClear(GL_COLOR_BUFFER_BIT);
		if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			while(weaponTimer > 60.0f / settings.playerFirerate)
			{
				weaponTimer -= 60.0f / settings.playerFirerate;
				playerBullets.addBullet(glm::vec2(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y), 0.0f, 2500.0f, weaponTimer);
			}
		}
		else
		{
			if(weaponTimer > 60.0f / settings.playerFirerate)
			{
				weaponTimer = 60.0f / settings.playerFirerate;
			}
		}
		deltaTime /= settings.subframes;
		for(size_t i = 0; i < settings.subframes; ++i)
		{
			enemies.update(deltaTime, window, enemyBullets, player.position);
			enemyBullets.update(deltaTime, window);
			checkPlayerCollisions();
			if(player.getHealth() <= 0)
			{
				return {Status::replace, std::make_unique<GameOver>(new MainState(settings), score)};
			}
			playerBullets.update(deltaTime, window);
			size_t destroyed = checkEnemyCollisions();
			score += destroyed;
			shake += destroyed;
		}
		player.setPosition(glm::vec2(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y));
		player.render();
		playerBullets.render();
		enemies.render();
		enemyBullets.render();
		if(settings.ghost)
		{
			scoreText.updateText("Bullets:" + std::to_string(enemyBullets.getBulletCount()));
			healthText.updateText("frametime:" + std::to_string(lastFrameTime) + "ms");
		}
		else
		{
			healthText.updateText("health:" + std::to_string(player.getHealth()));
			scoreText.updateText("Score:" + std::to_string(score));
		}
		healthText.render();
		scoreText.setPosition({window.getSize().x - scoreText.getWidth(), 0.0f});
		scoreText.render();
		if(enemies.getEndOfEnemies() && enemies.getEnemyCount() == 0)
		{
			return {Status::replace, std::make_unique<LevelComplete>(new MainState(settings), new MainState(settings), score)};
		}
		lastFrameTime = std::chrono::duration_cast<std::chrono::duration<float, std::milli>>(std::chrono::steady_clock::now() - start).count();
		window.display();
	}
	return {Status::pop, nullptr};
}

void MainState::checkPlayerCollisions()
{
	size_t hits = enemyBullets.removeIfColliding(player);
	if(settings.ghost)
	{
		return;
	}
	for(size_t i = 0; i < hits; ++i)
	{
		shake += 10;
		player.hit();
	}
}

size_t MainState::checkEnemyCollisions()
{
	for(auto& enemy : enemies.getEntities())
	{
		std::basic_string<size_t> indices_to_remove;
		size_t index = 0;
		for(auto& bullet : playerBullets.getEntities())
		{
			if(enemy.collidesWith(bullet))
			{
				enemy.hit();
				indices_to_remove += index;
				if(enemy.getHealth() <= 0)
				{
					break;
				}
			}
			++index;
		}
		playerBullets.removeIndices(indices_to_remove);
	}
	return enemies.removeDestroyed();
}

std::pair<State::Status, std::unique_ptr<State>> GameOver::run(sf::Window& window)
{
	pgl::storage::ubo ubo;
	ubo.bindBufferBase(0);
	ubo.reserve<glm::mat4>(2, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
	title.setSize(40.0f);
	text.setSize(40.0f);
	restartText.setSize(24.0f);
	static_cast<glm::mat4*>(ubo.getMappedPtr())[1] = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	static_cast<glm::mat4*>(ubo.getMappedPtr())[0] = glm::ortho<float>(0, window.getSize().x, window.getSize().y, 0, 0.0f, 10.0f);
	window.setMouseCursorVisible(true);
	while(window.isOpen())
	{
		sf::Event event;
		while(window.pollEvent(event))
		{
			switch(event.type)
			{
			case sf::Event::Closed:
				return {Status::pop, nullptr};
				break;
			case sf::Event::KeyPressed:
				switch(event.key.code)
				{
				case sf::Keyboard::Key::R:
					return {State::replace, std::move(m_nextState)};
				default:
					break;
				}
				break;
			case sf::Event::Resized:
				glViewport(0, 0, event.size.width, event.size.height);
				static_cast<glm::mat4*>(ubo.getMappedPtr())[0] = glm::ortho<float>(0, event.size.width, event.size.height, 0, 0.0f, 10.0f);
			default:
				break;
			}
		}
		glClear(GL_COLOR_BUFFER_BIT);
		title.setPosition({(window.getSize().x / 2 - title.getWidth() / 2), (window.getSize().y / 2 - title.getHeight() / 2) - text.getHeight() * 2});
		title.render();
		text.setPosition({(window.getSize().x / 2) - text.getWidth() / 2, window.getSize().y / 2 - text.getHeight() / 2});
		text.render();
		restartText.setPosition({(window.getSize().x / 2) - restartText.getWidth() / 2, (window.getSize().y / 2) + text.getHeight()});
		restartText.render();
		window.display();
	}
	return {Status::pop, nullptr};
}

std::pair<State::Status, std::unique_ptr<State>> LevelComplete::run(sf::Window& window)
{
	ubo.bindBufferBase(0);
	ubo.reserve<glm::mat4>(2, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
	title.setSize(40.0f);
	text.setSize(40.0f);
	nextLevelText.setSize(24.0f);
	restartText.setSize(24.0f);
	static_cast<glm::mat4*>(ubo.getMappedPtr())[1] = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	static_cast<glm::mat4*>(ubo.getMappedPtr())[0] = glm::ortho<float>(0, window.getSize().x, window.getSize().y, 0, 0.0f, 10.0f);
	window.setMouseCursorVisible(true);
	while(window.isOpen())
	{
		sf::Event event;
		while(window.pollEvent(event))
		{
			switch(event.type)
			{
			case sf::Event::Closed:
				return {Status::pop, nullptr};
				break;
			case sf::Event::KeyPressed:
				switch(event.key.code)
				{
				case sf::Keyboard::Key::R:
					return {State::replace, std::move(restartLevel)};
				default:
					break;
				}
				break;
			case sf::Event::MouseButtonPressed:
				return {State::replace, std::move(nextLevel)};
				break;
			case sf::Event::Resized:
				glViewport(0, 0, event.size.width, event.size.height);
				static_cast<glm::mat4*>(ubo.getMappedPtr())[0] = glm::ortho<float>(0, event.size.width, event.size.height, 0, 0.0f, 10.0f);
			default:
				break;
			}
		}
		glClear(GL_COLOR_BUFFER_BIT);
		title.setPosition({(window.getSize().x / 2 - title.getWidth() / 2), (window.getSize().y / 2 - title.getHeight() / 2) - text.getHeight() * 2});
		title.render();
		text.setPosition({(window.getSize().x / 2) - text.getWidth() / 2, window.getSize().y / 2 - text.getHeight() / 2});
		text.render();
		restartText.setPosition({(window.getSize().x / 2) - restartText.getWidth() / 2, (window.getSize().y / 2) + text.getHeight()});
		restartText.render();
		nextLevelText.setPosition({(window.getSize().x / 2) - nextLevelText.getWidth() / 2, (window.getSize().y / 2) + restartText.getHeight() + text.getHeight() + nextLevelText.getHeight() / 2});
		nextLevelText.render();
		window.display();
	}
	return {Status::pop, nullptr};
}

std::pair<State::Status, std::unique_ptr<State>> MainMenu::run(sf::Window& window)
{
	pgl::storage::ubo ubo;
	ubo.bindBufferBase(0);
	ubo.reserve<glm::mat4>(2, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
	static_cast<glm::mat4*>(ubo.getMappedPtr())[1] = glm::lookAt(glm::vec3(0, 0, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	static_cast<glm::mat4*>(ubo.getMappedPtr())[0] = glm::ortho<float>(0, window.getSize().x, window.getSize().y, 0, 0.0f, 10.0f);
	std::chrono::steady_clock clock;
	TextRenderer gameTitleText(GL_STATIC_DRAW, "{SI Proto}");
	gameTitleText.setSize(48.0f);
	TextRenderer startText(GL_STATIC_DRAW, "Press LMB to start");
	startText.setSize(24.0f);
	while(window.isOpen())
	{
		sf::Event event;
		while(window.pollEvent(event))
		{
			switch(event.type)
			{
			case sf::Event::MouseButtonPressed:
				return {State::Status::replace, std::make_unique<MainState>(settings)};
				break;
			case sf::Event::Closed:
				return {State::pop, nullptr};
				break;
			case sf::Event::Resized:
				glViewport(0, 0, event.size.width, event.size.height);
				static_cast<glm::mat4*>(ubo.getMappedPtr())[0] = glm::ortho<float>(0, event.size.width, event.size.height, 0, 0.0f, 10.0f);
				break;
			default:
				break;
			}
		}
		glClear(GL_COLOR_BUFFER_BIT);
		gameTitleText.setPosition({(window.getSize().x / 2) - gameTitleText.getWidth() / 2, 50.0f});
		startText.setPosition({(window.getSize().x / 2) - startText.getWidth() / 2, window.getSize().y - 50.0f});
		gameTitleText.render();
		if(std::chrono::duration_cast<std::chrono::seconds>(clock.now().time_since_epoch()).count() % 2)
		{
			startText.render();
		}
		window.display();
	}
	return {State::pop, nullptr};
}
