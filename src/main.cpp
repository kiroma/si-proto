#include <SFML/Window.hpp>
#include <iostream>
#include <string>

#include "settings.hpp"
#include "states.hpp"

int main(int argc, char** argv)
{
	Settings settings;
	std::cout.sync_with_stdio(false);
	for(int i = 1; i < argc; ++i)
	{
		if(std::string(argv[i]) == "-f")
		{
			settings.framerate = std::stoul(argv[++i]);
		}
		else if(std::string(argv[i]) == "-impossible")
		{
			settings.enemyFireRate += 666.0f * std::stoul(argv[++i]);
		}
		else if(std::string(argv[i]) == "-armageddon")
		{
			settings.enemiesPerSecond += 15.0f * std::stoul(argv[++i]);
		}
		else if(std::string(argv[i]) == "-god")
		{
			settings.playerHealth += 5000;
		}
		else if(std::string(argv[i]) == "-ivan")
		{
			settings.playerFirerate += 9600.0f * std::stoul(argv[++i]);
		}
		else if(std::string(argv[i]) == "-subframes")
		{
			settings.subframes = std::stoul(argv[++i]);
		}
		else if(std::string(argv[i]) == "-shakes")
		{
			settings.shake.strength = std::stof(argv[++i]);
		}
		else if(std::string(argv[i]) == "-shakea")
		{
			settings.shake.attenuation = std::stof(argv[++i]);
		}
		else if(std::string(argv[i]) == "-ghost")
		{
			settings.ghost = true;
		}
	}
	sf::ContextSettings defaultContext(0, 0, 4, 4, 5, sf::ContextSettings::Attribute::Core);
#ifndef NDEBUG
	defaultContext.attributeFlags |= sf::ContextSettings::Attribute::Debug;
#endif //NDEBUG
	sf::Window window(sf::VideoMode(800, 600), "si-proto", sf::Style::Default, defaultContext);
	window.setFramerateLimit(settings.framerate);
	if(!gladLoadGL())
	{
		throw std::runtime_error("glad initialization failed");
	}
#ifndef NDEBUG
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#endif
	glDebugMessageCallback(pgl::MessageCallback, NULL);
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glEnable(GL_BLEND);
	glEnable(GL_MULTISAMPLE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	ResourceManager globalManager;
	ResourceManager::initGlobalManager(globalManager);
	std::vector<std::unique_ptr<State>> states;
	states.emplace_back(std::make_unique<MainMenu>(settings));
	while(!states.empty())
	{
		State& current = *states.back();
		auto result = current.run(window);
		switch(result.first)
		{
		case State::replace:
			states.pop_back();
			[[fallthrough]];
		case State::push:
			states.emplace_back(std::move(result.second));
			break;
		case State::pop:
			states.pop_back();
			break;
		}
	}
}
