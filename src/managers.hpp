#pragma once

#include "pgl.hpp"
#include "resources.hpp"
#include "settings.hpp"
#include "sprites.hpp"
#include <array>
#include <random>
#include <vector>
#include <string_view>

template <class E>
class EntityManager
{
protected:
	pgl::Program& program;
	pgl::vao vao;
	pgl::storage::vbo vbo;
	pgl::storage::ebo ebo;
	pgl::storage::ssbo ssbo;
	pgl::Texture& texture;
	std::vector<E> entities;

public:
	EntityManager(const std::array<GLfloat, 16> square) :
		program(ResourceManager::getProgram(ResourceManager::Program::instancedOptimized)),
		texture(ResourceManager::getTexture(ResourceManager::Texture::basic))
	{
		vao.bind();
		vbo.bufferData(square, 0);
		static constexpr std::array<GLuint, 6> squareIndices{
		0, 1, 2,
		2, 3, 0};
		ebo.bufferData(squareIndices, 0);
		vao.bindEBO(ebo);
		program.use();
		vbo.bind();
		program.setAttrib<GLfloat>(0, 2, GL_FLOAT, GL_FALSE, 4, 0);
		program.setAttrib<GLfloat>(1, 2, GL_FLOAT, GL_FALSE, 4, 2);
	}
	auto& getEntities()
	{
		return entities;
	}
	void render()
	{
		if(entities.empty())
		{
			return;
		}
		vao.bind();
		
		texture.bind();
		program.use();
		if(entities.size() * sizeof(glm::mat3x2) > ssbo.getCapacity())
		{
			ssbo.reserve<glm::mat3x2>(entities.size() * 2, GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_WRITE_BIT);
		}
		ssbo.bindBufferBase(0);
		glm::mat3x2* ptr = static_cast<glm::mat3x2*>(ssbo.getMappedPtr());
		size_t i = 0;
		for(const auto& enemy : entities)
		{
			enemy.getCompressedModel(ptr[i++]);
		}
		glDrawElementsInstanced(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0, entities.size());
	}
};

class BulletManager : public EntityManager<Bullet>
{
	unsigned int skip = 0;
	static constexpr std::array<GLfloat, 16> square{
	-1.0f, -1.0f, 17.0f / 64.0f, 0.0f,
	1.0f, -1.0f, 19.0f / 64.0f, 0.0f,
	1.0f, 1.0f, 19.0f / 64.0f, 16.0f / 64.0f,
	-1.0f, 1.0f, 17.0f / 64.0f, 16.0f / 64.0f};

public:
	BulletManager() :
		EntityManager(square)
	{
	}
	void addBullet(const glm::vec2 position, const float rotation, const float speed = 2500.0f, const float deltaTime = 0.0f);
	void update(const float deltaTime, const sf::Window& window);
	void removeIndices(std::basic_string_view<size_t> indices_to_remove);
	size_t removeIfColliding(const BoundingBox& collider);
	unsigned int getBulletCount() const
	{
		return entities.size();
	}
};

class EnemyManager : public EntityManager<Enemy>
{
	struct EnemySpawn
	{
		float spawnTime;
		float spawnPosition;
		float rotation;
		float fireRate;
		bool operator<(const EnemySpawn& rhs)
		{
			return this->spawnTime < rhs.spawnTime;
		}
		bool operator>(const EnemySpawn& rhs)
		{
			return this->spawnTime > rhs.spawnTime;
		}
	};
	std::vector<EnemySpawn> spawns;
	std::vector<EnemySpawn>::const_iterator iterator;
	pgl::storage::ssbo ssbo;
	float levelTimer = 0.0f;
	Settings settings;
	std::mt19937 mt;
	bool endOfEnemies = false;
	static constexpr std::array<GLfloat, 16> square{
	-1.0f, -1.0f, 0.0f / 64.0f, 17.0f / 64.0f,
	1.0f, -1.0f, 8.0f / 64.0f, 17.0f / 64.0f,
	1.0f, 1.0f, 8.0f / 64.0f, 21.0f / 64.0f,
	-1.0f, 1.0f, 0.0f / 64.0f, 21.0f / 64.0f};

public:
	EnemyManager(const Settings& settings = Settings(), const std::string& path = "");
	Enemy& addEnemy(const glm::vec2 position, const float rotation, const float fireRate);
	void update(const float deltaTime, const sf::Window& window, BulletManager& bullets, const glm::vec2& fireAt);
	unsigned int removeDestroyed();
	bool getEndOfEnemies() const;
	size_t getEnemyCount() const;
};
