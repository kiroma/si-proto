#pragma once
#include <SFML/Window.hpp>
#include <glm/glm.hpp>

struct BoundingBox
{
	glm::vec2 position;
	glm::vec2 size;
	[[nodiscard]] constexpr bool collidesWith(const BoundingBox& other) const
	{
		return (position.x - size.x < other.position.x + other.size.x)
		& (position.x + size.x > other.position.x - other.size.x)
		& (position.y - size.y < other.position.y + other.size.y)
		& (position.y + size.y > other.position.y - other.size.y);
	}
};

class Entity : public BoundingBox
{
protected:
	float rotation;
	float s, c;

public:
	Entity() = default;
	template <typename T = BoundingBox, typename = std::enable_if_t<std::is_convertible_v<T, BoundingBox>>>
	constexpr Entity(T&& hitbox, float rotation) :
		BoundingBox(std::forward<T>(hitbox)),
		rotation(rotation),
		s(glm::sin(rotation)),
		c(glm::cos(rotation))
	{
	}
	constexpr glm::mat3x2 getCompressedModel() const
	{
		return {glm::vec2{c * size.x, s * size.x},
		{-s * size.y, c * size.y},
		{position.x, position.y}};
	}
	constexpr void getCompressedModel(glm::mat3x2& model) const
	{
		model = getCompressedModel();
	}
	constexpr void getModel(glm::mat4& model) const
	{
		model = getModel();
	}
	constexpr glm::mat4 getModel() const
	{
		return {c * size.x, s * size.x, 0, 0,
				-s * size.y, c * size.y, 0, 0,
				0, 0, 1, 0,
				position.x, position.y, 0, 1};
	}
	template <typename T = glm::vec2, typename = std::enable_if_t<std::is_convertible_v<T, glm::vec2>>>
	constexpr void setPosition(T&& position)
	{
		this->position = std::forward<T>(position);
	}
	template <typename T = float>
	constexpr void setRotation(T&& rotation)
	{
		this->rotation = std::forward<T>(rotation);
		s = glm::sin(this->rotation);
		c = glm::cos(this->rotation);
	}
	template <typename T = glm::vec2, typename = std::enable_if_t<std::is_convertible_v<T, glm::vec2>>>
	constexpr void setScale(T&& scale)
	{
		size = std::forward<T>(scale);
	}
};
