#include "managers.hpp"
#include "resources.hpp"

#include <fstream>
#include <glm/gtc/type_ptr.hpp>

void BulletManager::addBullet(const glm::vec2 position, const float rotation, const float speed, const float deltaTime)
{
	++skip;
	entities.emplace_back(position, rotation, speed).update(deltaTime);
}

void BulletManager::update(const float deltaTime, const sf::Window& window)
{
	const auto end = entities.end() - skip;
	for(auto iter = entities.begin(); iter != end; ++iter)
	{
		iter->update(deltaTime);
	}
	skip = 0;
	const BoundingBox windowBox{{0, 0}, {window.getSize().x, window.getSize().y}};
	entities.erase(std::remove_if(entities.begin(), entities.end(), [&windowBox](const Bullet& b) { return !b.collidesWith(windowBox); }), entities.end());
}

void BulletManager::removeIndices(std::basic_string_view<size_t> indices_to_remove)
{
	auto newEnd = entities.end();
	for(auto iter = indices_to_remove.crbegin(); iter != indices_to_remove.crend(); ++iter)
	{
		entities[*iter] = std::move(*(--newEnd));
	}
	entities.erase(newEnd, entities.end());
}

size_t BulletManager::removeIfColliding(const BoundingBox& collider)
{
	auto newEnd = std::remove_if(entities.begin(), entities.end(), [collider](const BoundingBox& bullet){return bullet.collidesWith(collider);});
	auto oldEnd = entities.end();
	entities.erase(newEnd, oldEnd);
	return std::distance(newEnd, oldEnd);
}

EnemyManager::EnemyManager(const Settings& settings, const std::string& path) :
	EntityManager(square),
	settings(settings)
{
	if(!path.empty())
	{
		std::ifstream ifs(path);
		if(!ifs.is_open())
		{
			throw std::runtime_error("Could not open \"" + path + "\"!");
		}
		for(EnemySpawn spawn; ifs >> spawn.spawnTime >> spawn.spawnPosition >> spawn.rotation >> spawn.fireRate;)
		{
			spawns.push_back(std::move(spawn));
		}
		std::sort(spawns.begin(), spawns.end());
		iterator = spawns.cbegin();
	}
}

Enemy& EnemyManager::addEnemy(const glm::vec2 position, const float rotation, const float fireRate)
{
	return entities.emplace_back(position, rotation, fireRate);
}

void EnemyManager::update(const float deltaTime, const sf::Window& window, BulletManager& bullets, const glm::vec2& fireAt)
{
	levelTimer += deltaTime;
	size_t skip = 0;
	if(settings.ghost || spawns.empty())
	{
		while(levelTimer >= 1.0f / settings.enemiesPerSecond)
		{
			levelTimer -= 1.0f / settings.enemiesPerSecond;
			addEnemy(glm::vec2(mt() % window.getSize().x, -16.0f), std::uniform_real_distribution(glm::radians(-20.0f), glm::radians(20.0f))(mt), settings.enemyFireRate).update(levelTimer);
			++skip;
		}
	}

	else
	{
		while(iterator->spawnTime <= levelTimer)
		{
			if(iterator == spawns.cend())
			{
				break;
			}
			addEnemy(glm::vec2(iterator->spawnPosition * window.getSize().x, -16.0f), glm::radians(iterator->rotation), iterator->fireRate);
			++iterator;
		}
		if(iterator == spawns.end())
		{
			endOfEnemies = true;
		}
	}
	for(size_t i = 0; i < entities.size() - skip; ++i)
	{
		auto& enemy = entities[i];
		enemy.update(deltaTime);
		while(enemy.isFiring())
		{
			glm::vec2 diff = fireAt - enemy.position;
			constexpr float straight = glm::radians(90.0f);
			float angle = std::atan2(diff.y, diff.x) + straight;
			enemy.fired();
			bullets.addBullet(enemy.position, angle, 400.0f, enemy.getFiringTime());
		}
	}
	const BoundingBox windowBox{{0, 0}, {window.getSize().x, window.getSize().y}};
	entities.erase(
	std::remove_if(entities.begin(), entities.end(), [&windowBox](const Enemy& e) { return !e.collidesWith(windowBox); }), entities.end());
}

unsigned int EnemyManager::removeDestroyed()
{
	const auto newEnd = std::remove_if(entities.begin(), entities.end(), [](const Enemy& e) { return e.getHealth() <= 0; });
	const auto destroyed = entities.end() - newEnd;
	entities.erase(newEnd, entities.end());
	return destroyed;
}

bool EnemyManager::getEndOfEnemies() const
{
	return endOfEnemies;
}

size_t EnemyManager::getEnemyCount() const
{
	return entities.size();
}
