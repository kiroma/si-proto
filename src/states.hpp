#pragma once
#include <chrono>
#include <random>

#include "managers.hpp"
#include "settings.hpp"
#include "sprites.hpp"
#include "text.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <memory>

class State
{
public:
	enum Status
	{
		push,
		pop,
		replace
	};
	virtual std::pair<Status, std::unique_ptr<State>> run(sf::Window& window) = 0;
	virtual ~State() = default;
};

class MainMenu : public State
{
	Settings settings;
public:
	MainMenu(Settings settings) : settings(settings)
	{}
	std::pair<Status, std::unique_ptr<State>> run(sf::Window& window) override;
};

class GameOver : public State
{
	std::unique_ptr<State> m_nextState;
	TextRenderer title{TextRenderer(GL_STATIC_DRAW, "YOU LOOSE")};
	TextRenderer text{TextRenderer(GL_STATIC_DRAW, "")};
	TextRenderer restartText{TextRenderer(GL_STATIC_COPY, "PRESS R TO RESTART THE LEVEL")};

public:
	GameOver(State* nextState, size_t score) :
		m_nextState(nextState), text(GL_STATIC_DRAW, "SCORE: " + std::to_string(score))
	{
	}
	std::pair<Status, std::unique_ptr<State>> run(sf::Window& window) override;
};

class LevelComplete : public State
{
	std::unique_ptr<State> restartLevel;
	std::unique_ptr<State> nextLevel;
	pgl::storage::ubo ubo;
	TextRenderer title{TextRenderer(GL_STATIC_DRAW, "YOU WIN")};
	TextRenderer text;
	TextRenderer nextLevelText{TextRenderer(GL_STATIC_DRAW, "PRESS LMB TO ADVANCE")};
	TextRenderer restartText{TextRenderer(GL_STATIC_DRAW, "PRESS R TO RESTART LEVEL")};

public:
	LevelComplete(State* restart, State* next, size_t score) :
		restartLevel(restart), nextLevel(next), text(GL_STATIC_DRAW, "SCORE: " + std::to_string(score))
	{
	}
	std::pair<Status, std::unique_ptr<State>> run(sf::Window& window) override;
};

class MainState : public State
{
	Settings settings;
	Player player;
	//sf::Music backgroundMusic; //TODO: Add music
	BulletManager playerBullets;
	BulletManager enemyBullets;
	EnemyManager enemies;
	pgl::storage::ubo sharedUbo;
	sf::Clock clock;
	float weaponTimer = 0.0f;
	// 	float enemySpawnTimer = 0.0f;
	std::mt19937 mt;
	unsigned int score = 0;
	TextRenderer healthText;
	TextRenderer scoreText;
	float lastFrameTime = 0.0f;
	float shake = 0.0f;
	void checkPlayerCollisions();
	size_t checkEnemyCollisions();

public:
	MainState(Settings settings = Settings());
	std::pair<Status, std::unique_ptr<State>> run(sf::Window& window) override;
};
