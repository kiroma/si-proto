#pragma once
#include "pgl.hpp"
#include <glm/glm.hpp>
#include <string_view>

class TextRenderer
{
	pgl::Program& program;
	pgl::Program& backgroundProgram;
	pgl::vao vao, backgroundVao;
	pgl::storage::vbo vbo;
	pgl::storage::vbo backgroundVbo;
	pgl::data::ebo ebo, backgroundEbo;
	pgl::Texture& texture;
	unsigned int indicesToDraw;
	glm::vec2 position;
	glm::mat4 model, bgModel;
	float textSize = 16.0f;
	float letterSpacing = 1.2f;
	const GLenum usage;
	glm::vec4 bgcolour = {0.0f, 0.0f, 0.0f, 0.0f};
	size_t characters;
	static constexpr glm::vec2 square[4]{
	{-0.0f, -0.0f},
	{1.0f, -0.0f},
	{1.0f, 1.0f},
	{-0.0f, 1.0f}};
	static constexpr GLuint squareIndices[6]{
	0, 1, 2,
	2, 3, 0};
	void updateModel();

public:
	TextRenderer(const GLenum usage, const std::string& text = "", glm::vec2 position = {0.0f, 0.0f});
	void setPosition(glm::vec2 newPosition);
	void setSize(const float size);
	float getWidth() const;
	float getHeight() const;
	void setBackgroundColour(glm::vec4 bgcolour);
	void render();
	void updateText(std::string text);
};
