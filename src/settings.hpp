#pragma once

struct Settings
{
	unsigned long framerate = 120;
	float enemyFireRate = 60.0f;
	float playerFirerate = 2700.0f;
	float enemiesPerSecond = 5.0f;
	int playerHealth = 3;
	size_t subframes = 2;
	struct
	{
		float strength = 4.0f;
		float attenuation = 32.0f;
	} shake;
	bool ghost = false;
};
