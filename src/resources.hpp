#pragma once
#include "pgl.hpp"
#include <SFML/Audio.hpp>
#include <SFML/Graphics/Image.hpp>
#include <cassert>

class ResourceManager
{
public:
	ResourceManager();
	ResourceManager(ResourceManager const&) = delete;
	void operator=(ResourceManager const&) = delete;
	enum class Texture : size_t
	{
		basic = 0,
		alphabet = 1
	};
	enum class Program : size_t
	{
		basic = 0,
		instanced = 1,
		textureless = 2,
		instancedOptimized = 3,
	};
	enum class Sound
	{

	};
	static pgl::Texture& getTexture(const ResourceManager::Texture texture);
	static pgl::Program& getProgram(const Program program);
	static sf::SoundBuffer& getSound(const Sound sound);
	static void initGlobalManager(ResourceManager& manager)
	{
		globalInstancePtr = &manager;
	}

private:
	static ResourceManager& get()
	{
		assert(globalInstancePtr != nullptr);
		return *globalInstancePtr;
	}
	static inline ResourceManager* globalInstancePtr = nullptr;
	std::vector<pgl::Texture> textures;
	std::vector<pgl::Program> programs;
	std::vector<sf::SoundBuffer> sounds;
};
