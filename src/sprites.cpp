#include "sprites.hpp"
#include "resources.hpp"
#include <glm/gtc/type_ptr.hpp>

Player::Player(int health) :
	Entity({glm::vec2(0.0f), glm::vec2(1.0f) * 24.0f}, 0.0f),
	program(ResourceManager::getProgram(ResourceManager::Program::instanced)),
	texture(ResourceManager::getTexture(ResourceManager::Texture::basic)),
	health(health)
{
	vao.bind();
	vbo.bind();
	ssbo.reserve<glm::mat4>(1, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
	std::vector<GLfloat> square{
	-1.0f, -1.0f, 0.0f / 64.0f, 0.0f / 64.0f,
	1.0f, -1.0f, 16.0f / 64.0f, 0.0f / 64.0f,
	1.0f, 1.0f, 16.0f / 64.0f, 16.0f / 64.0f,
	-1.0f, 1.0f, 0.0f / 64.0f, 16.0f / 64.0f};
	vbo.bufferData(square, GL_STATIC_DRAW);
	ebo.bind();
	std::vector<GLuint> squareIndices{
	0, 1, 2,
	2, 3, 0};
	ebo.bufferData(squareIndices, GL_STATIC_DRAW);
	vao.bindEBO(ebo);

	program.use();
	program.setAttrib<GLfloat>(0, 2, GL_FLOAT, GL_FALSE, 4, 0);
	program.setAttrib<GLfloat>(1, 2, GL_FLOAT, GL_FALSE, 4, 2);
}

void Player::render()
{
	vao.bind();
	program.use();
	texture.bind();
	getModel(*reinterpret_cast<glm::mat4*>(ssbo.getMappedPtr()));
	ssbo.bindBufferBase(0);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
}

int Player::getHealth() const
{
	return health;
}

void Player::hit()
{
	--health;
}

Enemy::Enemy(const glm::vec2 position, const float rotation, float fireRate) :
	Entity({position, glm::vec2(1.0f, 0.5f) * 32.0f}, rotation),
	fireRate(fireRate)
{
}

void Enemy::update(const float deltaTime)
{
	firingClock += deltaTime;
	setPosition(position + (glm::normalize(glm::vec2(-s, c)) * deltaTime * speed));
}

int Enemy::getHealth() const
{
	return health;
}

bool Enemy::isFiring() const
{
	return (firingClock > 60.0f / fireRate);
}

void Enemy::fired()
{
	firingClock -= 60.0f / fireRate;
}

float Enemy::getFiringTime()
{
	return firingClock;
}

void Enemy::hit()
{
	--health;
}
