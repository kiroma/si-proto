#include "text.hpp"
#include "resources.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

void TextRenderer::updateModel()
{
	model = glm::translate(glm::mat4(1), glm::vec3(position.x, position.y, 0));
	bgModel = model;
	model = glm::scale(model, glm::vec3(1.0f, 7.0f / 5.0f, 0.0f) * textSize);
	bgModel = glm::scale(bgModel, glm::vec3((1.0f * letterSpacing * characters) - (letterSpacing - 1.0f), 7.0f / 5.0f, 0.0f) * textSize);
}

TextRenderer::TextRenderer(const GLenum usage, const std::string& text, glm::vec2 position) :
	program(ResourceManager::getProgram(ResourceManager::Program::basic)),
	backgroundProgram(ResourceManager::getProgram(ResourceManager::Program::textureless)),
	texture(ResourceManager::getTexture(ResourceManager::Texture::alphabet)),
	position(position),
	usage(usage)
{
	vao.bind();
	updateText(text);
	vbo.bind();
	program.use();
	program.setAttrib<GLfloat>(0, 2, GL_FLOAT, GL_FALSE, 4, 0);
	program.setAttrib<GLfloat>(1, 2, GL_FLOAT, GL_FALSE, 4, 2);
	backgroundVao.bind();
	backgroundVbo.bind();
	std::vector<glm::vec2> bgBox;
	for(const auto& vec : square)
	{
		bgBox.push_back(vec);
	}
	std::vector<GLuint> bgIndices;
	for(const auto& ind : squareIndices)
	{
		bgIndices.push_back(ind);
	}
	backgroundVbo.bufferData(bgBox, 0);
	backgroundEbo.bufferData(bgIndices, GL_STATIC_DRAW);
	backgroundProgram.use();
	backgroundProgram.setAttrib<GLfloat>(0, 2, GL_FLOAT, GL_FALSE, 2, 0);
	glUniform4fv(3, 1, glm::value_ptr(this->bgcolour));
}

void TextRenderer::setPosition(glm::vec2 newPosition)
{
	position = newPosition;
	updateModel();
}

void TextRenderer::setSize(const float size)
{
	textSize = size;
	updateModel();
}

float TextRenderer::getWidth() const
{
	return letterSpacing * textSize * characters;
}

float TextRenderer::getHeight() const
{
	return (7.0f / 5.0f) * textSize;
}

void TextRenderer::setBackgroundColour(glm::vec4 bgcolour)
{
	this->bgcolour = bgcolour;
	backgroundProgram.use();
	glUniform4fv(3, 1, glm::value_ptr(this->bgcolour));
}

void TextRenderer::render()
{
	backgroundVao.bind();
	backgroundEbo.bind();
	backgroundProgram.use();
	glUniformMatrix4fv(2, 1, GL_FALSE, glm::value_ptr(bgModel));
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
	vao.bind();
	program.use();
	texture.bind();
	ebo.bind();
	glUniformMatrix4fv(2, 1, GL_FALSE, glm::value_ptr(model));
	glDrawElements(GL_TRIANGLES, indicesToDraw, GL_UNSIGNED_INT, nullptr);
}

void TextRenderer::updateText(std::string text)
{
	characters = text.length();
	indicesToDraw = text.length() * 6;
	if(text.empty())
	{
		return;
	}
	vbo.bind();
	for(char& a : text)
	{
		if(a >= 'a')
		{
			if(a <= 'z')
			{
				a -= 'a' - 'A';
			}
			else
			{
				a -= '{' - 'a';
			}
		}
	}
	if(vbo.getCapacity() < text.length() * sizeof(glm::mat4))
	{
		std::vector<glm::vec4> box{};
		box.reserve(4 * text.size());
		for(size_t i = 0; i < text.size() * 2; ++i)
		{
			for(const glm::vec2 vec : square)
			{
				box.push_back(glm::vec4(vec.x + letterSpacing * i, vec.y, 0, 0));
			}
		}
		vao.bind();
		vbo.bufferData(box, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT, true);
		program.use();
		program.setAttrib<GLfloat>(0, 2, GL_FLOAT, GL_FALSE, 4, 0);
		program.setAttrib<GLfloat>(1, 2, GL_FLOAT, GL_FALSE, 4, 2);
		ebo.bind();
		std::vector<GLuint> boxIndices{};
		for(size_t i = 0; i < text.size() * 2; ++i)
		{
			for(auto a : squareIndices)
			{
				boxIndices.push_back(a + (i * 4));
			}
		}
		ebo.bufferData(boxIndices, usage);
	}
	glm::vec4* box = reinterpret_cast<glm::vec4*>(vbo.getMappedPtr());
	for(size_t i = 0; i < text.size(); ++i)
	{
		char letterX = (text[i] - ' ') % 10;
		char letterY = (text[i] - ' ') / 10;
		float charWidth = 5.0f / 64.0f;
		float charHeight = 7.0f / 64.0f;
		float charPosX = 6.0f / 64.0f;
		float charPosY = 8.0f / 64.0f;
		box[(i * 4) + 0].z = letterX * charPosX;
		box[(i * 4) + 0].w = letterY * charPosY;
		box[(i * 4) + 1].z = (letterX * charPosX) + charWidth;
		box[(i * 4) + 1].w = letterY * charPosY;
		box[(i * 4) + 2].z = (letterX * charPosX) + charWidth;
		box[(i * 4) + 2].w = (letterY * charPosY) + charHeight;
		box[(i * 4) + 3].z = letterX * charPosX;
		box[(i * 4) + 3].w = (letterY * charPosY) + charHeight;
	}
	vbo.flushBuffer();
	updateModel();
}
