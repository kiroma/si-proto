#include "resources.hpp"
#include <iostream>

ResourceManager::ResourceManager()
{
	std::cout << "Loading resources..." << std::endl;
	{
		sf::Image player;
		player.loadFromFile("res/spaceship.png");
		textures.emplace_back();
		textures[0].load(GL_RGBA2, player.getSize().x, player.getSize().y, GL_RGBA, GL_UNSIGNED_BYTE, player.getPixelsPtr());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, 0.0f);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_LOD, 0.0f);
	}
	{
		sf::Image alphabet;
		alphabet.loadFromFile("res/alphabet.png");
		textures.emplace_back();
		textures[1].load(GL_RGBA2, alphabet.getSize().x, alphabet.getSize().y, GL_RGBA, GL_UNSIGNED_BYTE, alphabet.getPixelsPtr());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, 0.0f);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_LOD, 0.0f);
	}
	programs.reserve(4);
	#ifndef NDEBUG
	if(GLAD_GL_ARB_gl_spirv)
	{
		programs.emplace_back(pgl::Program(
		pgl::Shader(GL_VERTEX_SHADER, "res/basic_opt.spv", true),
		pgl::Shader(GL_FRAGMENT_SHADER, "res/basic_opt.spv", true)));
		programs.emplace_back(pgl::Program(
		pgl::Shader(GL_VERTEX_SHADER, "res/instanced_opt.spv", true),
		pgl::Shader(GL_FRAGMENT_SHADER, "res/instanced_opt.spv", true)));
		programs.emplace_back(pgl::Program(
		pgl::Shader(GL_VERTEX_SHADER, "res/textureless_opt.spv", true),
		pgl::Shader(GL_FRAGMENT_SHADER, "res/textureless_opt.spv", true)));
		programs.emplace_back(pgl::Program(
		pgl::Shader(GL_VERTEX_SHADER, "res/instancedCompressed_opt.spv", true),
		pgl::Shader(GL_FRAGMENT_SHADER, "res/instancedCompressed_opt.spv", true)));
	}
	else
	#endif
	{
		pgl::Shader basicFrag(GL_FRAGMENT_SHADER, "res/basic.frag");
		programs.emplace_back(pgl::Program(
		pgl::Shader(GL_VERTEX_SHADER, "res/basic.vert"),
		basicFrag));
		programs.emplace_back(pgl::Program(
		pgl::Shader(GL_VERTEX_SHADER, "res/instanced.vert"),
		basicFrag));
		programs.emplace_back(pgl::Program(
		pgl::Shader(GL_VERTEX_SHADER, "res/textureless.vert"),
		pgl::Shader(GL_FRAGMENT_SHADER, "res/textureless.frag")));
		programs.emplace_back(pgl::Program(
		pgl::Shader(GL_VERTEX_SHADER, "res/instancedCompressed.vert"),
		basicFrag));
	}
	std::cout << "Finished loading resources." << std::endl;
}

pgl::Texture& ResourceManager::getTexture(const Texture texture)
{
	return get().textures[static_cast<size_t>(texture)];
}

pgl::Program& ResourceManager::getProgram(const Program program)
{
	return get().programs[static_cast<size_t>(program)];
}

sf::SoundBuffer& ResourceManager::getSound(const Sound sound)
{
	return get().sounds[static_cast<size_t>(sound)];
}
